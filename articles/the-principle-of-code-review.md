---
title: "Code Review 的一些原则"
date: 2019-06-11T18:40:50+08:00
tags:
- code review
---

Code review 已经是软件开发工作中非常重要[^1]的一部分了，不过回顾之前的实习经历，发现还是走了很多弯路。

首先 code review 是为了提高代码质量，减少代码中潜在的缺陷，同时也是公司信息共享的一部分。对于小公司来说，code review 更是规避了一些风险，避免工程师的单点情况出现。

所以任何关于代码格式的问题都与 code review 的目的无关，这些都应该尽可能用自动化工具来解决，而不是在 code review 的时候反复讨论。

在 code review 过程中，**双方**都应该做到：

- 每一个回复都尽可能地完整

信息缺失的回复只会平白无故地增加沟通次数，降低效率。

**代码提交者**则应该：

- 在每个提交中都做清晰的说明，方便别人快速理解代码
- 自动化测试没法覆盖的，要进行手动测试并说明场景和结果

在提交时要假设对方不明白相关的背景和目的，这样不仅方便以后追溯，也让更多人能够参与到 code review 中来，同时还能加深自己对代码的理解。

对于手动测试这种没有放入版本控制工具中的内容，在 code review 时进行说明能够方便大家给出更多的测试建议。

**代码复查者**则应该：

- 找到代码的妙处，给出积极性的鼓励，即使代码需要返工

这一点在 code review 提供改进意见之外，充分考虑了工程师的感受，可见人在软件工程的重要性。不过这也算是 LinkedIn[^2] 这种典型硅谷公司为了应对多元文化所探索出来的解决方案吧。

[^1]: 陈皓. 2014. 从 CODE REVIEW 谈如何做技术. https://coolshell.cn/articles/11432.html
[^2]: Szczepan Faber. 2017. LinkedIn's Tips for Highly Effective Code Review. https://thenewstack.io/linkedin-code-review/
