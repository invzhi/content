---
title: "深度工作"
date: 2018-12-01T15:34:23+08:00
tags:
- book
draft: true
---

类似的效率书还挺多的，我觉得这种书只能给出一个参考，真正在落实阶段还是需要自己有意识地不断找到适合自己的方法来提高效率。

这本书强调了深度工作的价值，把其他工作概括为浮浅工作，比如冗长的会议、整理收件箱等等。

为什么现在大部分的工作是浮浅工作呢？

- 公司并不能量化打断了员工的深度工作有什么影响
- 对于员工来说，做深度工作还是浮浅工作都没有得到明确的反馈意见，所以员工倾向于做当下最简单易行的决定，也就是做浮浅工作

然而，真正能够培养自己核心能力的还是深度工作，那种能够全身心投入创作的状态。所以说深度工作的能力是非常重要的，当然有些职位的确是避免不了这种情况，比如销售的工作就不可避免地遭遇各种干扰。

这本书从四个大的规则来讲深度工作：工作要深入、拥抱无聊、远离社交媒体、摒弃浮浅。

## 工作要深入
书中列举了很多的建议来保持专注，不过我觉得更重要的是书中描述意志力的理论：人的意志力是有限的，所以培养深度工作习惯的关键并不在于意志力，而是在于把进入状态所消耗的意志力最小化。

工作本身就是一件反人性的事情，有人会觉得工作就是需要很强的意志力来支撑，其实并不是这样的，更好的做法应该是顺着人性去调整自己的习惯，让自己更平滑地进入状态。

## 拥抱无聊
高度集中注意力是一种需要训练的技能。一旦你的大脑习惯了随时分心，即使你想要专注的时候也难以摆脱这种习惯。

这个观点就和浅薄一书里的观点一致了，这里引用一句话：

> Don't underestimate the value of doing nothing.

比较有意思的一个建议就是在身体劳作而心智空闲的时候，可以把注意力集中到一件明确的专业难题上，经典的例子就是 Alan Turing 喜欢在跑步的时候思考问题。

## 远离社交媒体
这个规则实际上和上一条的核心观点差不多，不过作者选择网络工具的方法值得一提，就是多关注你得到了什么，而不仅仅是关注失去了什么。

大部分人沉迷信息流的一大原因就是怕错过一些信息，然而那些信息对我们来说并没有太大的价值。

## 摒弃浮浅
除了增加进行深度工作的时间，减少浮浅工作的时间也非常重要，这就需要不断用自动化等各种手段把浮浅工作最小化。
