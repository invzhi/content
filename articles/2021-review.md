---
title: "魔幻的岁月回顾，2021"
date: 2022-01-09T12:46:14+08:00
tags:
- review
---

毕业工作2年多了，打算在新的年份开始之际，尝试建立起自己的复盘框架和routine。在回顾过去一年得失的同时，明确未来的目标，同时不断地修正前进的方向。

## 健康 

### 运动

环境真的很重要，今年出差换着住了很多酒店，但还是喜欢刚来成都时住的一个就在健身房边上的房间，就算加班到很晚也会在睡觉之前跑步。不过后来换的酒店都不尽人意，没有坚持跑步整个人的状态就差了不少。

![Habits Report for Sport in 2021](https://static.invzhi.com/images/Habits_Sport_2021.jpg)

6月定居成都之后，陆续住了2个小区，周边的环境都非常适合跑步。借着工作gap的契机就把习惯记录的APP也捡了起来，半年下来感觉身体没有达到非常棒的状态，但也没有因为日常久坐出现什么问题。

### 饮食

8月和9月陆续拔了两侧的智齿，给未来的口腔健康打下了良好的基础。现在养成了每天晚上刷牙前用牙线的好习惯，拔除智齿之后使用牙线也更加顺畅了。

饮食和运动习惯都是一件很长期的事情，现在已经有了少吃碳水多摄入蔬菜的意识，但是在执行的时候还是比较松散，缺乏系统性的一些知识和实操。

### 计划

接下来的一年继续把身体健康放在首要的位置，也希望能够在饮食和运动方面有一些输入，慢慢应用到自己身上，让自己的身体状态越来越好。新的一年也打算探索一下冥想。

## 心智

### 工作

工作上2021前后经历了两次大的变动，在2020年底加入橙心优选感受了一把公司核心部门的刺激，在2021年6月选择离开了滴滴。回头再看那段日子虽然丰富了个人经历，能力上也有提升，其实并没有创造真正的用户价值，反而在助推资本做骚扰用户的事情。旁观滴滴内部这场追涨杀跌的盛况后，深刻意识到要用长期的视角做长期的事情。

重新找工作前后也经历了自信心的变化，从刚离职的信心知足到gap四个月后的一丝慌张。 休息了几个月没写代码毕竟还是有一点虚，不过面试的过程中一步步坚定了自己的信心。新的一年里打算定期出去面试，在寻找机会的同时也了解一下市场情况。

最后在11月加入了做东南亚业务的小型外企，工作氛围还行，团队内部积累的文档都使用英文，上下班也不打卡，Slack & Confluence办公。最重要的是一般情况下都没有加班，每天有足够的时间跑步阅读提升自己。

### 自我

离开大厂的选择也让我接触一些其他的同行，拥有了一些样本可以学习和对比。这个过程中越来越意识到信息质量的重要性，同时也感受到了自己现有的瓶颈。每天上班习惯先打开Hacker News，但是很多感兴趣的文章只是丢到了Pocket，并没有很好的吸收。尽管可以比较流畅地阅读英文内容，但是在阅读速度上还有很大的进步空间。

习惯说「我不知道」，长期下来就能感受到这种回路的力量。不欺骗自己，承认自己不知道的，如果好奇就做一番research，慢慢地就能比身边的人多了解很多东西。希望在新的一年里继续保持。

### 计划

决定方向比埋头苦干重要得多，要在正确的方向上前进。接下来在职业上最想做的事情就是希望可以不依赖工资养活自己。不是通过创办企业利用劳动力或者资本的杠杆，而是利用互联网产品去放大自己的影响力。但是要小心自己的决策也会被放大，所以在往前冲的同时也要提高自己的决策能力。

## 关系

### 亲密关系

2021开始了全新的一段亲密关系，虽然有些磕磕碰碰，不过是至今为止最舒适最幸福的一段关系了。反思之前的亲密关系也发现了自己的很多问题，比如暴力沟通、把自己的想法强加给对方等等。5月份的时候在小蒲的督促下读了「爱的进化论[^1]」，还打算好好读一下「非暴力沟通[^2]」。

### 父母关系

父母关系估计是大家都有共鸣的难题，最近刚好在这方面有了一些输入[^3]。父母的认知也是需要子女去耐心引导的，其实我们完全可以把父母当成小孩子去认真对待，多一点耐心和认真的态度，计划在接下来的一年里慢慢实践起来。

## 财富

### 现金流

![Income Report in 2021](https://static.invzhi.com/images/beancount_Income_2021.png)

工资还是主要的收入来源，2021第一次拿了完整的3个月年终奖，不得不说还是很香的。

![Expenses Report in 2021](https://static.invzhi.com/images/beancount_Expenses_2021.png)

租房占了支出的很大一部分，定居成都之后也顺势开始了解买房相关的事情。团队里的同事也经常会讨论成都的房地产，希望我在做这个重大决策之前能够把背后的逻辑都了解清楚，做一个清醒的人。

### 投资

2021算是在投资上摸爬滚打的第二年。不亏损永远是第一要义，在实操的过程中也踩了一些坑。很感谢张潇雨老师的个人投资课和有知有行的投资第一课这样的内容，非常喜欢那句「投资成功是我们变成更好的人之后的自然结果」。

## 2021

离开第一份工作，在一个全新的城市重新定居下来。伴随着换工作的契机，重新审视了自我，找回了方向。

这就是我的2021。

[^1]: Alain de Botton. 2017. 爱的进化论.  https://book.douban.com/subject/27073878/
[^2]: Marshall Rosenberg. 2009. 非暴力沟通 https://book.douban.com/subject/3533221/
[^3]: 余晟. 2016. 改善与父母关系的几点建议 https://mp.weixin.qq.com/s/64rC7RlML9Y-M7t2bpAqzw